var form = (function() {
  var me = {};


  me.init = function() {

    removeDefaultRequredStates();

    $(document).on('submit','.js-form',beforeFormSubmit);
    $(document).on('click','.js-clear-form',clearForm);
    $(document).on('blur','.js-form input.error',checkInput);
    $("body").on('form.submited',onFormSubmit);
  };

  function removeDefaultRequredStates() {
    $('.js-form').each(function(){
      var $self = $(this);
      $self.find('input[required]').addClass('required').removeAttr('required');
      $self.addClass('js-required');
    })
  }

  $.fn.shake = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        $(this).css("position","relative"); 
        for (var x=1; x<=intShakes; x++) {
        $(this).animate({left:(intDistance*-1)}, (((intDuration/intShakes)/4)))
    .animate({left:intDistance}, ((intDuration/intShakes)/2))
    .animate({left:0}, (((intDuration/intShakes)/4)));
    }
    });
  return this;
  };


  function beforeFormSubmit(e) {
    e.preventDefault();
    var $form = $(this);

    if (! $form.hasClass('js-required')) {
      removeDefaultRequredStates();
    }

    $form.find('input.required').each(checkInput);
    if (! $form.find('input.required.error').length) {
      $("body").trigger('form.submited', $form);
    }
  }

  function checkInput() {
    var $input = $(this);
    if ($input.val().length) {
      $input.removeClass('error');
    } else {
      $input.addClass('error');
      $input.shake(3,15,600);
    }
  }

  function clearForm(e) {
    e.preventDefault();
    var $form = $(this).closest('form');
    $form.find('.form__input').removeClass('error').val('');
    $form.find('.form__textarea').removeClass('error').val('');
    if ($form.closest('.modal').length) {
      $form.closest('.modal').modal('hide');
    }
  }

  function onFormSubmit(e, form) {
    if ($(form).closest('.modal').length) {
      $(form).closest('.modal').modal('hide');
    }
    if ($('#modal-success').length) {
      $('#modal-success').modal('show');
    }
  }

  return me;
}());