var timeline = (function() {
  var me = {};


  me.init = function() {

    $(window).on('load resize', timeleineSlider);
  };

  function timeleineSlider() {
    var $winndowSize = $(window).width();

    if ( $winndowSize < 600) {
      var $timeline = $('#timeline').iosSlider({
        desktopClickDrag: true,
        snapToChildren: true,
        snapVelocityThreshold: 6,
        verticalSlideLockThreshold: 3,
        responsiveSlideContainer:true,
        responsiveSlides: true,
        navSlideSelector: $('.slider-bullet'),
        startAtSlide: 1,
        autoSlide:false,
        autoSlideTransTimer: 800,
        onSliderLoaded: slideChange,
        onSlideChange: slideChange
      });

      $('.slider-bullet').on('click', function() {
        var $self = $(this);
        $self.addClass('radioAnimation');
        var animTime = ($(this).css('animation-duration').split("s"))[0]*1000;
        setTimeout(function(){ $self.removeClass('radioAnimation');}, animTime);
      });
    } else {
      $('#timeline').iosSlider('destroy');
    }
  }

  function slideChange(args){
    var thisSlider = args.data.obj;
    thisSlider.find('.slider-bullet.active').removeClass('active');
    thisSlider.find('.slider-bullet').eq(args.currentSlideNumber-1).addClass('active');
    
    var features = thisSlider.closest('.sl').find('.feat');
    features.filter('.active').removeClass('active');
    setTimeout(function(){
      features.eq(args.currentSlideNumber-1).addClass('active');
    },200)
  }

  return me;
}());