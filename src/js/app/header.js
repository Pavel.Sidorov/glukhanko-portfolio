var header = (function() {
  var me = {},
      $stickyOffset = 0;

  me.init = function() {

    $(document).on('click','#toggleHeaderMenu', toggleHeader);

    // липкий хедер 
    if (('#js-sticky-header').length) {
      stickyHeaderOffset();
      $('body').on('slider.height.upd',stickyHeaderOffset);

      // fix on 600-1200px
      var scrollspy = $('#js-sticky-header').scrollspy({
        mode: 'horizontal',
        onTick: function(element, position) {
          var $windowWidth = $(window).width();
          if ($(element).hasClass('fixed') && $windowWidth>= 600 && $windowWidth < 1020) {
            $("#js-sticky-header").css('left',(-position.left+"px"));
          } else {
            $("#js-sticky-header").css('left',0);
          }
        },
        container: window
      });
    
      // прилипание хедера
      $(window).on('load resize scroll', stickyHeader);
    }
  };

  // need offset?
  function stickyHeaderOffset() {
    $stickyOffset = 0;
    $('.js-stick-header-after-me').each(function(){
      var $selfOffset = $(this).offset().top + $(this).outerHeight();
      if ($selfOffset > $stickyOffset) {
        $stickyOffset = $selfOffset;
      }
    });
    // console.log('stickyHeaderOffset calculated, its = '+$stickyOffset);
    return false;
  }

  
  function toggleHeader() {
    var $self = $(this),
        $headerMenu = $('#headerMenu'),
        $header = $('#headerMenu').closest('header.header');
    if ($headerMenu.length) {
      if ($header.hasClass('opened')) {
        $headerMenu.slideUp(200, function(){
          $header.removeClass('opened');
          $self.removeClass('opened');
        });
      } else {
        $headerMenu.slideDown(200, function(){
          $self.addClass('opened');
          $header.addClass('opened');
        });
      }
    }
    return false;
  }

  function stickyHeader() {
    var $window = $(window),
        $header = $('#js-sticky-header');

    if ($window.width() > 600) {
      if ($window.scrollTop() > $stickyOffset && (! $header.hasClass('fixed'))) {
        if ($stickyOffset > 100) {
          $header
            .hide()
            .addClass('fixed')
            .slideDown(200);
        } else {
          $header.addClass('fixed');
        }
      }

      if ($window.scrollTop() < $stickyOffset && ($header.hasClass('fixed'))) {
        $header.removeClass('fixed');
      } 

      // fix position bug on 600-1020
    } else {
      $header.removeClass('fixed');
    }
    return false;
  }

  return me;
}());