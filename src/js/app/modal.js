var modal = (function() {
  var me = {};


  me.init = function() {
    $('.modal').on('show.bs.modal', beforeModalShown);
  };

  function beforeModalShown(){
    var $self = $(this),
        $window = $(window);

    // iOS check...ugly but necessary
    // if( navigator.userAgent.match(/iPhone|iPad|iPod/i) ) {
    //   $self.css({
    //       position: 'absolute',
    //       marginTop: $(window).scrollTop() + 'px',
    //       bottom: 'auto'
    //   });

    //   setTimeout( function() {
    //       $self.find('.modal-backdrop').css({
    //           position: 'absolute', 
    //           top: 0, 
    //           left: 0,
    //           width: '100%',
    //           height: Math.max(
    //               document.body.scrollHeight, document.documentElement.scrollHeight,
    //               document.body.offsetHeight, document.documentElement.offsetHeight,
    //               document.body.clientHeight, document.documentElement.clientHeight
    //           ) + 'px'
    //       });
    //   }, 0);
    // }

    // if ($window.width()>=600) {
    if (true) {
      $self.css({
        visibility: 'hidden',
        display:    'block'
      });
      var modalHeight = $self.find('.modal-dialog').outerHeight();
      // console.log(modalHeight);
      $self.css({
        visibility: 'visible',
        display:    'none'
      });
      
        if (modalHeight < $window.height()) {
        $self.find('.modal-dialog').css({
          'margin-top': - modalHeight / 2,
          'position': 'absolute',
          'top': '50%'
        });
        }
    }
  }
  return me;
}());