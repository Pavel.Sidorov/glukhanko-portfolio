var slider = (function() {
  var me = {},
      $sliderInitHeight = 0,
      mainSlider = '';

  me.init = function() {
    $sliderInitHeight = +$('#slider').parent().css('min-height').split('px')[0];

    $(window).on('load resize', buildSlider);
    $(window).on('orientationchange', function(){
      setTimeout(function() {
        $('#slider').iosSlider('update');
      }, 1000);
    });
  };

  function buildSlider() {
    var $sliderHeight = $sliderInitHeight;
    if ( $(window).height() > $sliderHeight) {
      $sliderHeight = $(window).height();
    }

    if ($sliderHeight > 941) {
      $sliderHeight -= 306;
    }

    $('#slider').parent().css('height', $sliderHeight+'px');
    $('body').trigger('slider.height.upd');
    $('#slider .slide').css('height', $sliderHeight+'px');

    if (!mainSlider.length) {
      initSlider();
    }

    return false;
  }

  function initSlider() {
    // setTimeout(function(){
      mainSlider = $('#slider').iosSlider({
        infiniteSlider: true,
        desktopClickDrag: true,
        autoSlideHoverPause: false,
        scrollbar: true,
        snapToChildren: true,
        snapVelocityThreshold: 6,
        verticalSlideLockThreshold: 3,
        responsiveSlideContainer:true,
        responsiveSlides: true,
        navSlideSelector: $('.slider-bullet'),
        navPrevSelector: $('.slider__arrow--left'),
        navNextSelector: $('.slider__arrow--right'),
        startAtSlide: 1,
        autoSlide:true,
        autoSlideTransTimer: 800,
        autoSlideTimer: 3000,
        onSliderLoaded: sliderLoaded,
        // onSliderLoaded: slideChange,
        onSlideChange: slideChange
      });
    // }, 10);

    $('.slider-bullet').on('click', function() {
      var $self = $(this);
      $self.addClass('radioAnimation');
      var animTime = ($(this).css('animation-duration').split("s"))[0]*1000;
      setTimeout(function(){ $self.removeClass('radioAnimation');}, animTime);
    });

    // если есть блок подписей, то они кликабельны
    $('.sl__features .feat').on('click', function (e) {
      e.preventDefault();
      var slideNum = 1 + $('.feat').index($(this));
      mainSlider.iosSlider('goToSlide', slideNum);
    });

    $(document).keydown( function(e) {
      if(e.keyCode === 37) {
        // left arrow
        mainSlider.iosSlider('prevSlide');
      }
      if(e.keyCode === 39) {
        // right arrow
        mainSlider.iosSlider('nextSlide');
      }
    });
  }

  function sliderLoaded(args){
    slideChange(args);
    $('#slider').parent().removeClass('preloader');
  }


  function slideChange(args){
    var thisSlider = args.data.obj;
    thisSlider.find('.slider-bullet.active').removeClass('active');
    thisSlider.find('.slider-bullet').eq(args.currentSlideNumber-1).addClass('active');
    
    var features = thisSlider.closest('.sl').find('.feat');
    features.filter('.active').removeClass('active');
    setTimeout(function(){
      features.eq(args.currentSlideNumber-1).addClass('active');
    },200)
  }

  return me;
}());