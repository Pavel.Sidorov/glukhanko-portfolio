var works = (function() {
  var me = {},
      // $proportion = 1.4,
      $proportion = 1.618,
      $works = $('#works'),
      $container = $('#worksContainer'),
      $bigHeight = 0,
      $smallHeight = 0,
      initIsBestChecked = $('#switch-best-works').prop('checked'),
      $dataFilter = {
        best: $('#switch-best-works').prop('checked'),
        web: false,
        brand: false,
        photo: false
      };


  me.init = function() {

    // isotope init
    $container.isotope({
      itemSelector: '.work-item',
      resizable: false,
      masonry: { columnWidth: $container.width() / 3 }
    });

    // items layout
    $(window).on('load resize', onLoadResize);

    // switch
    if ($('.js-switch-toggle').length) {
      $('.js-switch-toggle').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.switch').find('.switch__input').not(':checked').eq(0).trigger("click");
      });

      // toggle data on switch changes
      $('#switchBest').on('change', function(e) {
        e.preventDefault();
        $dataFilter.best = !$dataFilter.best;
        dataFilterDefault($dataFilter.best);
      });
    }

    // toggle data on filter click
    $(document).on('click','#filterWeb', {key: 'web'}, onFilterClicked);
    $(document).on('click','#filterBrand', {key: 'brand'}, onFilterClicked);
    $(document).on('click','#filterPhoto', {key: 'photo'}, onFilterClicked);


    // reload layout
    $works.on('items.filter', function(e, data){

      var filterArray = [];
      for(var key in data) {
        if (data[key] && key !== 'best') {
          filterArray.push(key);
        }
      }

      defineBigWorks(filterArray, data.best);

      if (! filterArray.length) {
        $container.isotope({ filter: '.barakObama'});
      } else {
        if (data.best) {
          $container.isotope({ filter: '.best.'+filterArray.join(', .best.')});
        } else {
          $container.isotope({ filter: '.'+filterArray.join(', .')});
        }
      } 
    });

    // =========================
    // ISOTOPE report
    // =========================
    // $container.isotope( 'on', 'layoutComplete',
    //   function( isoInstance, laidOutItems ) {
    //     console.log( 'Isotope layout completed on ' +
    //       laidOutItems.length + ' items' );
    //   }
    // );

  };


  // layout
  function onLoadResize() {
    initIsBestChecked = $('#switch-best-works').prop('checked');
    dataFilterDefault(initIsBestChecked);
    $container.isotope({
      // update columnWidth to a percentage of container width
      masonry: { columnWidth: ($container.width()) / 3 }
    });
  }

  // define what works are big
  function defineBigWorks(filterArray, isBest) {
    filterArray = typeof filterArray !== 'undefined' ? filterArray : [];
    isBest = typeof isBest !== 'undefined' ? isBest : false;
    var elems = $('.work-item');
    elems.removeClass('big');
    var filteredElems = [];

    if (filterArray.length) {
      if (isBest) {
        elems = elems.filter('.best');
      }

      for (var i = filterArray.length - 1; i >= 0; i--) {
        filteredElems = $.merge(filteredElems,elems.filter('.'+filterArray[i]));
      };

      filteredElems = $.unique(filteredElems);

      for (var i = 0; i <= filteredElems.length - 1; i+=6) {
        $(filteredElems[i+1]).addClass('big');
        $(filteredElems[i+3]).addClass('big');
      };
    }
    calculateWorksSizes();
  }


  function calculateWorksSizes() {
    var $windowWidth = $(window).width(),
        $containerWidth = 1020;

    if ($windowWidth < 600 || $windowWidth > 1200) {
      $containerWidth = $windowWidth;
    }

    if ($windowWidth < 320) {
      $containerWidth = 320;
    }

    if ($windowWidth < 600) {
      $bigHeight = Math.round($containerWidth/$proportion);
      $smallHeight = $bigHeight;
    } else {
      $bigHeight = Math.round($containerWidth*(2/3)/$proportion);
      $smallHeight = (($bigHeight - 40) / 2).toFixed(1);
    }

    addSizesToElements();
    return false;
  }

  function addSizesToElements() {
    $('.work-item .work-item__inner').css('height', $smallHeight + 'px');
    $('.work-item.big .work-item__inner').css('height', $bigHeight + 'px');
    return false;
  }

  function onFilterClicked(e) {
    e.preventDefault();
    var $self = $(this),
        key = e.data.key;
    $self.closest('.filter').find('.filter__item').removeClass('active')
    $self.addClass('active');
    $dataFilter.web = false;
    $dataFilter.brand = false;
    $dataFilter.photo = false;
    $dataFilter[key] = true;
    $works.trigger('items.filter',$dataFilter);
  }

  function dataFilterDefault(isBest) {
    isBest = typeof isBest !== 'undefined' ? isBest : false;
    $dataFilter = {
      best: isBest,
      web: true,
      brand: true,
      photo: true
    };
    $('#filterWeb').addClass('active');
    $('#filterBrand').addClass('active');
    $('#filterPhoto').addClass('active');
    $works.trigger('items.filter',$dataFilter);
  }

  return me;
}());